import z3

a = z3.Re('a')
b = z3.Re('b')
c = z3.Re('c')
r1 = z3.Concat(a, z3.Star(z3.Concat(b, a))) # a(ba)*
r2 = z3.Concat(z3.Star(z3.Concat(a, b)), a) # (ab)*a
r3 = z3.Union(r2, z3.Concat(z3.Star(z3.Concat(a, c)), a)) # ((ab)*a)|((ac)*a)
solver = z3.Solver()
solver.add(z3.Intersect(r1, r3) == r1)
result = solver.check()
if z3.sat == result:
  print(f'Not equivalent; counterexample: {solver.model()}')
else:
  print(result)
  print('Equivalent!')
