import z3
from role_analyzer import regex_to_z3_expr
import sre_parse

node_location = z3.String('node_location')
node_running = z3.String('node_running')
location_regex = regex_to_z3_expr(sre_parse.parse('us-east-[\w]+'))

role = z3.And(z3.InRe(node_location, location_regex), node_running == z3.StringVal('fooapp'))
solver = z3.Solver()
solver.add(role)
result = solver.check()
if z3.sat == result:
  print(solver.model())
else:
  print(result)
  print('Equivalent!')