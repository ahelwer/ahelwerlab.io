Independent software engineering consultant with over a decade of professional experience.
Previously worked at Acceleware, Microsoft Azure Compute, Microsoft Azure Networking, and Microsoft Quantum.
BSc in Computer Science from the University of Calgary.
From Canada, currently based out of Atlanta, GA, USA.
TLA⁺ enthusiast!

Interested in hiring me for a contract?
Contact me [on LinkedIn](https://www.linkedin.com/in/ahelwer/) or email consulting@disjunctive.llc.
Work playing to my skills includes:
 * Formally specifying & model-checking your system with TLA⁺ [[1]](/post/2023-03-30-pseudocode/) [[2]](/post/2023-04-05-checkpoint-coordination/)
 * Formally proving the correctness of your system [[1]](/post/2020-04-05-lean-assignment/) [[2]](/post/2023-03-30-pseudocode/)
 * Assisting in the design of your distributed system or protocol [[1]](/post/2023-04-05-checkpoint-coordination)
 * Analyzing your probabilistic system or protocol with PRISM [[1]](/post/2020-04-15-probabilistic-distsys)
 * Writing tricky distributed systems code for your system's backend [[1]](/post/2023-04-05-checkpoint-coordination)
 * Validating your system's implementation with model-based testing
 * Analyzing your access control system with the Z3 theorem prover [[1]](/post/2018-02-13-z3-firewall) [[2]](/post/2022-01-19-z3-rbac)
 * Writing a tree-sitter grammar for your domain-specific language [[1]](/post/2023-01-11-tree-sitter-tlaplus)
 * Education in technical quantum computing concepts [[1]](https://youtu.be/F_Riqjdh2oM) [[2]](/post/2018-12-07-chsh) [[3]](/post/2019-12-21-quantum-chemistry) [[4]](/post/2020-12-06-sum-over-paths)
 * General development work on formal methods tools themselves [[1]](/post/2023-01-11-tree-sitter-tlaplus)

I have experience with C++, Java, C#, Rust, Golang, Python, and Kubernetes.
See my full résumé [here](/resume).
 
Email ahelwer@protonmail.com for personal correspondence.
I'm always happy to answer questions about my interests, especially quantum computing!

Outside of work I'm an avid climber; find tales & photos of my adventures at [outdoors.ahelwer.ca](https://outdoors.ahelwer.ca).

{{< figure src="/img/enchantments.jpg" >}}

All content on this website is licensed under [CC-BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/).
