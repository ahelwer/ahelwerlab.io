---
title: Buridan's ass, or why digital computers are impossible
date: 2022-10-20
draft: true
---

Digital computers impose discrete order on our chaotic, continuous world.
It should never cease to amaze that we've tricked small pockets of the universe into implementing a formal logical system.
Untold legions of brilliant people have dedicated their entire working lives to the marginal refinement of these pockets, and it occasionally behooves software engineers - sitting far, far up the enormous, teetering stack of abstraction - to marvel at the numerous impossibilities narrowly skirted for their benefit.
While error correction draws most of the spotlight, my favorite such impossibility is Buridan's principle, known in modern times as the arbiter problem.

## An Old Problem
