---
title: Google Groups has been left to die
subtitle: Where should the formal methods community move?
date: 2023-03-08
bigimg: [{src: "sunset.jpg", desc: "Sunset over the Gulf of Mexico seen from Naples, Florida, USA" }]
image: "/post/2023-03-08-google-groups/sunset.jpg"
---

An unusual topic for this blog, but worth boosting to a larger audience: Google Groups is dying.
Its epitaph is not yet inscribed on the [Killed by Google](https://killedbygoogle.com/) website, but the end is easily seen from here (although it should also be noted its death was called as early as [13 years ago](https://www.reddit.com/r/programming/comments/9y9qg/google_groups_is_dead_killed_by_spam/)).
The deficiencies in Google Groups search, supposedly Google's forte, have long been noted.
Lately though basic features have just stopped working.

Why care?
Google Groups is, for whatever reason, the de-facto standard community website in the formal methods community.
[TLA⁺](https://groups.google.com/g/tlaplus) uses it, [PRISM](https://groups.google.com/g/prismmodelchecker) uses it, [SMT-LIB](https://groups.google.com/g/smt-lib) uses it, and a number of other tools I could find at least have presences on the platform.
These communities take time to build: their value resides in the number of people who think of them first whenever they want to ask a question or just talk about these tools.
Many websites link to these groups or to specific answers.

What's broken?
Beyond search's perpetual brokenness, [Monospace fonts have just stopped rendering](https://groups.google.com/g/tlaplus/c/JGkgE2ul8Qk).
This makes code samples significantly more difficult to read.
The read/unread indicator is janky, marking threads you've visited and even your own posts & replies as unread by you.
Then (precipitating this post) messages or replies submitted to the group have started intermittently just self-deleting ([1](https://github.com/tlaplus/tlaplus/issues/796) [2](https://github.com/tlaplus/CommunityModules/issues/88)).
Not in a graceful way, either: the post will successfully be submitted, but then all that will show up is a deleted message.
Hope you didn't spend twenty minutes typing a thorough, detailed response!
My attempt to submit a link to this very post also deleted itself.

It's clear we ran afoul of the old lesson: don't build communities for long-lasting FOSS projects on proprietary infrastructure you don't control.
[We should not use Discord](https://drewdevault.com/2021/12/28/Dont-use-Discord-for-FOSS.html).
So what should we use?
I think modern online FOSS communities require four things:
 1. A group chatroom for low-stakes pseudo-realtime discussion
 2. A mailing list or forum for announcements & effortful discussions requiring multi-paragraph posts & responses; this should have an optional email interface
 3. A Q&A website similar to StackOverflow where the correct answer can be marked and elevated instead of staying buried in a long discussion
 4. Easy search function for all of the above, without having to create an account

The Alloy formal specification language is ahead of the game here: they've set up [their community site](https://alloytools.discourse.group/) on [Discourse](https://www.discourse.org/), a FOSS project ticking all of those feature boxes.
[Coq](https://coq.discourse.group/) has also moved to Discourse.
However, this doesn't have to be a one-stop shop!
One can imagine a Matrix, Mattermost or Zulip chat room in addition to an actual old-school mailing list or FluxBB forum.
The latter don't have specific Q&A functionality, but FluxBB at least seems to work for [the SPIN model checker](https://spinroot.com/fluxbb/) (and - not formal methods but very successful - [Arch Linux](https://bbs.archlinux.org/)).
[Coq](https://coq.zulipchat.com/), [Lean](https://leanprover.zulipchat.com/), and [Isabelle](https://isabelle.zulipchat.com/) use Zulip.
TLA⁺ had [a Mattermost instance](http://talk.tlapl.us/) at one point but that seems to have died.
It's also tempting to use [GitHub Discussions](https://docs.github.com/en/discussions) since all these projects are developed on GitHub anyway, but I think this tendency should be resisted.
GitHub is wonderful now but fundamentally proprietary, same as Google Groups, and FOSS software forges like [sourcehut](https://sourcehut.org/) are making great progress.
Moving development to FOSS software forges might happen within the next decade and it will be easier if the non-development community isn't also scattered in the process.

This experience has been instructive for me.
Once you start thinking on timescales of 5-10 years, communities built on proprietary software just don't pass muster.
Certainly self-hosted FOSS communities can die, but these are functions of community activity itself rather than the service they're hosted on.

### Discussions
 * [lobste.rs](https://lobste.rs/s/mt2p8g/google_groups_has_been_left_die)
 * [Hacker News](https://news.ycombinator.com/item?id=35070618)
 * [r/programming](https://old.reddit.com/r/programming/comments/11m02x6/google_groups_has_been_left_to_die/)

### Highlighted Comments
 * User [ttkciar on r/programming](https://old.reddit.com/r/programming/comments/11m02x6/google_groups_has_been_left_to_die/jbf79cc/) suggested the [Fossil software configuration management (SCM)](https://fossil-scm.org/home/doc/trunk/www/index.wiki) project. I was impressed with how quickly things loaded as I clicked around the linked website, which self-hosts the project itself.
 * User [dsr_ on Hacker News](https://news.ycombinator.com/item?id=35072523) suggested the D programming language's [custom community site](https://forum.dlang.org/), which is [open source](https://github.com/CyberShadow/DFeed), mailing-list-oriented, and also very fast.
 * User [andyc on lobste.rs](https://lobste.rs/s/mt2p8g/google_groups_has_been_left_die#c_omvlqu) (among others) provided a strong recommendation for Zulip. People seem to really like Zulip!
 * A number of users pointed out Google Groups is used for a lot of things within the Google ecosystem. This gives rise to the question of whether the Google Group product used for those things is different from the clearly withering Google Group public bulletin board functionality that is the topic of this post.

