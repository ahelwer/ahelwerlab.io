---
title: How I model networks in TLA⁺
date: 2024-01-11
tags: ["tlaplus", "distributed-systems", "formal-methods"]
bigimg: [{src: "palm-frond.jpg", desc: "A Silver Bismarck Palm frond at Naples Botanical Garden, Florida, USA" }]
image: "/post/2024-01-11-tla-network-modeling/palm-frond.jpg"
draft: true
---

Specifying network behavior is one of the earlier design problems new TLA⁺ users encounter.
Despite TLA⁺ seeing tremendous use in distributed systems, neither the language nor its standard module library define primitives for processes exchanging messages over a network.
In this post I'll detail the approach I usually take to solving this problem, refined across several paid contracts and personal projects.
I make no claim to originality; experienced TLA⁺ users have all likely come up with their own method largely resembling my own.
New users will find this useful though, and old hands might find it interesting to compare!

What does a TLA⁺ network model need?
Mostly I think it needs options!
There is a tradeoff between how adversarial the network model is and how quickly the model-check completes, so adversity should be parameterized.
What things should be parameterized?
How about:
- Ordered vs. unordered message delivery
- Whether messages can be lost
- Whether messages can be delivered multiple times

All of this should be encapsulated so the non-network part of the spec doesn't have to worry about these parameters, and can use a nice uniform API to send or receive messages.
How do we do it?

## Do we actually need to model the network?

First we should consider whether any of this is actually worthwhile.
You don't actually have to model the network if you don't want to!
It is perfectly valid to just have one process read or write the variables of another, direct-memory-access-style.
Usually it makes sense to write from the perspective of the receiving process, as it directly reads another process's variables then updates its own.
I used this approach in [my last post](/post/2023-11-01-tla-finite-monotonic/), for the `Gossip` action.
It's easy, it's simple, it's fast.
If you don't care about separating the send & receive steps in your model it works perfectly fine.
On the other hand if you're worried about your system remaining functional if some message is delivered twice, you might want to flesh out the network in more detail.

## The API

