---
title: Meditation
subtitle: Sadly my only weapon against the attention economy
date: 2020-08-29
bigimg: [{src: "redwood.jpg", desc: "A tree in Redwood National Park, CA, USA"}]
tags: ["practices"]
image: "/post/2020-08-29-meditation/redwood.jpg"
---

This isn't going to be a post about how adopting a several-thousand-year-old practice [can make you a better servant of capital](https://www.independent.co.uk/life-style/health-and-families/healthy-living/mindfulness-sells-buddhist-meditation-teachings-neoliberalism-attention-economy-a8225676.html).
Instead, let's talk about when I feel the lowest of the low.
It comes after spending any number of hours on my computer, maybe even a full day, endlessly circling around different websites in search of stimulation, the quick jolt that comes with learning an interesting fact or watching a funny short video or seeing someone get dunked on for having a bad political opinion.
I'll eventually wake into a state that could be identified as conscious thought, look back on the many (genuinely exciting!) things I wanted to learn or do that day, and contrast it with what I actually did.
What did I actually do? I honestly don't even know. I doubt I could list even three things I'd read or experienced the entire preceding eight hours.
At this moment I feel very bad. I know one of the ~30,000 days in my life is gone.
And I didn't get anything out of it.
Actually, I'm worse off, because spending your time in this way just begets even more time spent in this way.

I don't have any social media apps on my phone.
I deleted my twitter account.
I deleted my reddit account.
I don't have an instagram account.
My facebook account exists only to be part of some groups.
I use LeechBlock.
I redirect websites to 0.0.0.0 in my hosts file.
Everyone I know does most of these things.
Everyone I know still has trouble spending their time how they actually want to spend it.
If I were to write down a list of the things I find most important in life, endlessly circling around a handful of websites would not even be in consideration.

If I get stuck in the loop for more than a week or so my brain starts to eat itself.
There's a visceral knawing feeling.
My thoughts dwell on how to defeat this thing which is deleting hours from my life.

So far the only thing which has helped is mindfulness meditation.
After sitting for 10 or 20 minutes and focusing on my breath, my mind becomes quiet.
Traffic on the [default mode network](https://en.wikipedia.org/wiki/Default_mode_network) dies down a little bit.
I'm no longer instantly pulled into the online vortex upon encountering the slightest speedbump in my work.
Instead my mind just... stays quiet, and I keep the problem in my head.
This doesn't last forever.
I could probably meditate for 10 minutes out of every hour and still have spent more time on the things I actually wanted to spend time on, at the end of the day.

Deciding to meditate is hard.
The idea of spending ten minutes intentionally doing nothing runs against a lot of messaging I've internalized.
It's also very difficult to keep my attention on my breath instead of running away to consider whatever figment wanders through my thoughts.
There's an interesting metaphor I've heard.
Your mind is like a train station.
Thoughts think themselves - they are trains pulling into the station.
A train of thought will shortly pull out of the station, but you don't have to be on it.
Or maybe you just boarded it without thinking, then don't realize what you've done until you're a mile down the track.
No worries; you can always return to the station.

I decided to try mindfulness meditation after listening to the audio version of Robert Wright's book [Why Buddhism Is True](http://whybuddhismistrue.net/) while on a cross-country drive.
It's about how many beliefs in Buddhism - specifically Western Atheistic Buddhism - have support from modern psychology & neuroscience.
I wasn't motivated to start meditating by thinking it would help me in this struggle for my attention; the discovery was quite accidental.
I use the [Waking Up](https://www.wakingup.com) app.
Friends of mine also report good experiences with [Headspace](https://www.headspace.com/).

It's troubling I have to go to such lengths to live my life the way I think I want to live it.
Maybe this struggle is uniquely modern, maybe it's one for the ages.
I still have issues sometimes, especially before bed when my defenses are lower and I can get sucked into the loop instead of reading a good book or just going to sleep.
Still, for the first time in my life I've found an actually effective tool for resisting the attention economy.

## Further Reading & Watching

{{< youtube wf2VxeIm1no >}}
