---
title: Art & media I enjoyed in 2024
date: 2024-12-31
draft: true
tags: ["personal"]
bigimg: [{src: "wisteria.jpg", desc: "Wisteria growing on a flowering tree in Atlanta, Georgia, USA" }]
image: "/post/2024-05-28-tla-unicode/wisteria.jpg"
---

Here's a wrap-up of art & media I enjoyed this year.
I think taste in media (especially books) is some of the most personal information you can learn about somebody, so I'm putting myself out there more than normal with this - but hopefully some kindred spirits will find this & use it to connect!

I mostly read books & play video games, rarely watching films & shows.
I also subscribe to Clarkesworld and will give my favorite sci-fi short stories they published this year!
In each category I'll list the media in the chronological order of when I encountered it as the months passed.

## Books

[*Tomorrow, and Tomorrow, and Tomorrow*](https://en.wikipedia.org/wiki/Tomorrow,_and_Tomorrow,_and_Tomorrow) [2022] by Gabrielle Zevin -
A fairly fun fictional novel about some students who come together to build a video game company.
The characters played video games as forms of research into what art they could create (fun to pretend I am doing this).
The games they create would probably be fairly niche if they existed in reality but maybe the sort of games I would like.

[*The Hardware Hacker*](https://nostarch.com/hardwarehackerpaperback) [2019] by Andrew "bunnie" Huang -
This nonfiction book opened my eyes to the reality of hardware development and the dynamic world of Shenzhen.
Between reading this book and attending the Embedded Open Source Summit conference early in the year I'm thinking that embedded software development will be in my future.
One particularly interesting section of this book talks about open software & hardware and how the analogous culture in Shenzhen operates very differently.

[*The Odyssey*](https://www.emilyrcwilson.com/the-odyssey) [2018] translated by Emily Wilson, read by Claire Danes -
Listened to this as an audiobook.
The translation was wonderful but honestly I wasn't a fan of the story.
All the really iconic stuff (cyclops, sirens, scylla & charybdis, etc.) is told in recollection by a drunk Odysseus over two books in the middle.
The rest of the story is just Telemachus fucking around and Odysseus playing cringe undercover boss with his slaves.
The foreword on the history of The Odyssey and its translation was very interesting, though.

[*Annihilation*](https://en.wikipedia.org/wiki/Annihilation_(VanderMeer_novel)) [2014] by Jeff VanderMeer -
This was a re-read for me, since I loved it so much the first time I read it.
It's short and leaves you wanting to learn so much more about its world, although in many ways any explanation would undo so much of what makes this book special.
Much, much better than its film adaptation.

[*Distress*](https://en.wikipedia.org/wiki/Distress_(novel)) [1995] by Greg Egan -
Usually love everything by Greg Egan but this was a bit of a miss for me.
The material is interesting (anthrocosmology! anarchist island state grown from pirated coral biotech! competing physics theories of everything! widespread voluntary exploration of gender & even the autistic spectrum!) but plotwise it doesn't hold together very tightly.
The book turns into a tale of many competing & conflicting factions that I don't think plays to the author's writing strengths and sort of collapses under its own weight at some point.

[*Artificial Condition*](https://en.wikipedia.org/wiki/The_Murderbot_Diaries#Artificial_Condition_(2018)) [2018] by Marth Wells -
I've gotten hooked on the Murderbot Diaries universe and this was a fun short novel.
Short but not very memorable compared to the original; I struggle to recall the plot six months later.

[*Blackshirts & Reds*](https://citylights.com/city-lights-published/blackshirts-reds-rational-fascism/) [1997] by Michael Parenti -
Left-wing politics in the US largely comprises Anarchists and Trots, meaning you aren't likely to hear positive things about the USSR in those circles.
Among other segments of the left this book is usually brought up as a near-complete articulation of their alternative view, focusing on the heavy price paid by the USSR to defeat Nazi fascism and the rapid growth in living standards compared to Tsarist Russia, as well as positing that much of the success of the Western left in the first half of the 20th century can be attributed to the existence & thus threat of Soviet Communism.
The book also covers the post-Soviet years of the 1990s and the awful resulting conditions in many Eastern-European countries.
This was my second read because nothing really stuck the first time; this topic is basically a live wire in Western left-wing politics so I'll elide most of my actual thoughts on it but it was certainly interesting!

[*The New Essential Guide to Electronics in Shenzhen*](https://www.crowdsupply.com/machinery-enchantress/the-new-essential-guide-to-electronics-in-shenzhen) [2024] by Naomi Wu -
An updated version of the original book by Andrew "bunnie" Huang, this is a practical guide to navigating the Shenzhen electronics world, including new pages focusing on the experiences of women & queer-identifying people.
There is also a large flashcard-like section with lots of specialized Chinese vocabulary for components & their properties!

[*If We Burn: The mass protest decade and the missing revolution*](https://vincentbevins.com/book2/) [2023] by Vincent Bevins -
A follow-up to the author's excellent first book *The Jakarta Method*, this book covers the phenomenon of spontaneous/horizontalist/self-organizing protests born from the social media era of the 2010s.
This is the predominant method of organizing protests here in the US so I was particularly interested.
It analyzes ten case studies from around the world including Egypt, Ukraine, Honk Kong, Brazil, and others.
The author contends these protests are good at blowing holes in the political power structure but not good at persisting & filling them.
Often, reactionary elements take advantage.
Their focus on tactics that draw attention on US-developed social media can be to the detriment of actual on-the-ground victories.
This is one of many books I wish I could just insta-download into the brains of people I organize with.

[*Before the Storm: Barry Goldwater and the Unmaking of the American Consensus*](https://en.wikipedia.org/wiki/Rick_Perlstein#Before_the_Storm_(2001)) [2001] by Rick Perlstein -
This book purports to cover how US politics shifted massively to the right between the New Deal era of FDR and the rise of Ronald Reagan in the 80s.
It does an okay job I suppose, but past the midpoint it turned into a day-by-day account of the 1964 LBJ vs. Goldwater US presidential primaries & campaign.
Given that 2024 was a US presidential campaign year and knowledge of US presidential campaigns is a cognitohazard at baseline, I was totally over it and skipped to the end.

[*Palestine*](https://www.fantagraphics.com/products/palestine-hardcover) [2001] by Joe Sacco -
This is an unbelievable journalistic graphic novel illustrating the author's experiences witnessing the Israeli occupation of Palestine in the early 1990s.
It has been out of stock for the past year (for obvious reasons) but I managed to snag a copy from the local library.
As bad as the events detailed are, you realize the conditions depicted were those of 30 years ago and the occupation has advanced rapidly in the interim.
I am left wondering about the fates of the many characters with their dreams of freedom & independence.

[*Settlers: The Mythology of the White Proletariat*](https://pmpress.org/index.php?l=product_detail&p=679) [2014] by J. Sakai -
Another one of those books I wish I could insta-download into the brains of people I know, and one that totally upended my view of class structure of the US.
As I grow older, the times I encounter something that really shakes the base of my knowledge grow rare.
This book was one of those events; it brings a lot of questions I've been struggling with about why the US is the way it is into focus, and shines light on my experiences growing up in Canada where interaction with indigenous people is much more common than in the US.
The concept of settlerism and how it affects the character of both the population being displaced and the population doing the displacement has extraordinary explanatory power.
Read it!!!

## Favorite Clarkesworld Short Stories

In chronological order, here are my favorite sci-fi short stories I read in Clarkesworld in 2024.
Clarkesworld publishes nearly 100 short stories each year, so at a certain point memorability starts to matter just as much as quality.
Anyway, at the end of the year Clarkesworld runs a reader voting campaign to highlight communal favorites so you'll also be able to see those results soon.
All stories are freely available to read online but you should definitely [subscribe](https://clarkesworldmagazine.com/subscribe/)!

[*Just Another Cat in a Box*](https://clarkesworldmagazine.com/auslender_01_24/) by E.N. Auslender, Issue 208, January 2024 -
This was a strong start to the year, mostly because it was just very darkly funny.
A man wakes up from a resurrection pod, and I'll let you read the rest.

[*A Brief Oral History of the El Zopilote Dock*](https://clarkesworldmagazine.com/johnson_03_24/) by Alaya Dawn Johnson, Clarkesworld 210, March 2024 -
I think this one has an argument for winning best of the year.
It has some similarity in theme to *The Parable of the Sower*, but is a more realistic prediction of the trajectory of mass incarceration & border securitization in the US.
Anarchists will love it.

[*The Brotherhood of Montague St. Video*](https://clarkesworldmagazine.com/ha_05_24/) by Thomas Ha, Issue 212, May 2024 -
An ode to physical media & those who preserve it in a society where the digital world pervades every interaction.

[*The Best Version of Yourself*](https://clarkesworldmagazine.com/collier_07_24/) by Grant Collier, Issue 214, July 2024 -
What happens when we can use nanobots to modify peoples' brains in response to their desires?
You get joy jelly.

[*Molum, Molum, Molum the Scourge*](https://clarkesworldmagazine.com/larson_08_24/) by Rich Larson, Issue 215 - August 2024 -
A washed-up augmented gladiator needs to feed his implants and, against his better judgement, teams up with a younger fan on a heist.
The author had one of their prior short stories adapted on Love, Death & Robots, and I can see why!

