---
title: Taking my home work setup seriously
subtitle: Ergonomics & settling in for the long haul
date: 2020-08-14
lastmod: 2022-01-23
bigimg: [{src: "final-closeup.jpg"}]
tags: ["practices"]
image: "/post/2020-08-14-home-ergonomics/final-closeup.jpg"
aliases: ["/post/2020-08-09-home-ergonomics/"]
---

The headlines don't lie.
[Microsoft](https://www.crn.com/news/mobility/microsoft-extends-work-from-home-option-through-january), [Google](https://www.cnn.com/2020/07/27/tech/google-work-from-home-extension/index.html), [Amazon](https://www.theverge.com/2020/7/15/21326017/amazon-work-from-home-extend-january-2021-corporate), [Facebook](https://www.cnbc.com/2020/08/06/facebook-will-allow-employees-to-work-remotely-until-july-2021.html), and a whole host of other tech companies have announced employees will be working from home until early-mid 2021.
There are reasons to believe this will be pushed back; even if the [staggeringly ambitious](https://blogs.sciencemag.org/pipeline/archives/2020/04/15/coronavirus-vaccine-prospects) timelines for vaccine development are met, [a vaccine might not be a silver bullet](https://www.cbc.ca/player/play/1772035651845) and the pandemic could require management for the next 2-3 years.
As a software engineer in big tech's orbit, this means it's time to settle in for the long haul and take my home work setup seriously.

I've been very concerned with ergonomics ever since early-career wrist issues had me mousing with my non-dominant hand for six months.
One of my university friends had such terrible pain that he became a pioneer in [open-source speech recognition for programming by voice](https://livestream.com/internetsociety3/hopeconf/videos/131671656) (although his issues didn't turn out to be from repetitive stress injuries).
All of which is to say I haven't used a non-ergonomic keyboard or mouse in about a decade.
Still, my home setup, while decent, fell short in a few crucial areas.
It was time to make some improvements - I'm in this career for the long run.
Join me on this somewhat self-indulgent journey!

*Note: I added a section updated with my experiences in early 2022, check the end.*

# The Objective

My top-level aims were as follows:
 1. Create a setup where I'm happy & comfortable working for long periods of time
 2. Create a setup which will not cause injury in the mid to long term

To facilitate these aims, I chose these constraints:
 1. Focus on basic uncomplicated ergonomics, anchored in *adjustability*
 2. Prioritize *frugality* by buying used, and only buying features I definitely need
 3. Avoid falling into the *consumerist perfection trap*

### Ergonomics & Adjustability

When it comes to ergonomics, the science isn't complicated; here's how you get the biggest bang for your buck:
 1. Set the relative height of your chair & desk so that you type & mouse with straight wrists (not flexed up or down)
 2. Reduce wrist pronation (inward twisting) by using a split/tented keyboard and vertical mouse or trackball
 3. Ensure monitors are placed high enough so you can see them without sagging your neck forward (this is a big drawback with laptops)

All the rest of it - standing desks, exotic chair designs, desk treadmills, mechanical keyswitches, keyboard trays, gas spring monitor arms - are basically a huge waste of time & money if you can't nail these three basic things.
I'm not going to spend time on good posture because that falls in the domain of flossing & working out every day: if you want to do it, you'll do it.
Programmer's slouch can even be accomodated to a degree:

{{< youtube LXYLVbt7j7o >}}

Regarding adjustability, we've all probably heard the tale of how the USAF designed its airplane cockpits for "average" body dimensions, with the end result that [literally nobody exactly fit those designs](https://www.thestar.com/news/insight/2016/01/16/when-us-air-force-discovered-the-flaw-of-averages.html).
This informs our approach here: prioritizing adjustability over other concerns like "quality" or whatever. Everyone reading this post will have body dimensions purely unique to themselves, and mere inches can make a difference.

### Frugality

Buy used. Not complicated.
Computing equipment depreciates like milk left out in the sun, so you can snag perfectly good five-year-old equipment for 75-80% off MSRP (edit: as of early 2022 this is not true for GPUs).
Check Craigslist first (or Facebook Marketplace), then eBay.
I've been doing this for years and haven't been burned once.
Single-core CPU performance [has been flat for almost a decade now](https://www.cpubenchmark.net/year-on-year.html); you probably don't really need the latest & greatest to do your work.
Also, most ergonomic equipment is built like a tank and can easily stand the test of time & use.

The second component to frugality is much more subjective, but basically amounts to not buying what you don't really need.
A great example here is a [$25 generic metal bolt monitor stand vs. $300 gas spring monitor arms](https://youtu.be/__K4V8pFhf4) from Ergotron:

{{< youtube __K4V8pFhf4 >}}

If you're like me, once your monitors are in the appropriate position they're basically never touched again.
Buying a fancy gas spring stand would be a complete waste of money.
The example makes it sound simple, but it's really easy to forget this as you're watching flashy product videos.
Buying stuff is fun!
Which leads into our next constraint...

### The Consumerist Perfection Trap

This is best illustrated by a YouTube comment I found under [a Linus Tech Tips video](https://www.youtube.com/watch?v=LALQsqZP1nA) reviewing the very expensive Ergodox split mechanical keyboard:

![Youtube comment by user jakejakeboom: "I feel like if I invested in this keyboard, my life would inevitably spiral out of control. I'd fall into a vicious chase of ergonomic custom keyboard perfection. 6 months later I'd be under a bridge somewhere, frantically swapping key switches to scratch that itch that I'll never quite be able to reach."](ergodox-youtube-comment.PNG)

Humans are endlessly adaptable, for better or worse.
Before setting out, know this: you might sink days of research and thousands of dollars into your work setup, but it will never be quite perfect.
Just accept this. Live with it.
It's called [hedonic adaptation](https://en.wikipedia.org/wiki/Hedonic_treadmill).
Even if your monitors end up an inch too close to your face, remember that the whole setup is *massively* better than sitting at your kitchen table or couch with a laptop.
Buying things is fun, and spending money begets spending more money.
It's a good idea to let a setup sit for a couple weeks or a month before rushing off with further tweaks & improvements.

Inside us all there is a void.
People want to complete themselves and fill this void with spirituality, or hedonistic pursuits, or material things.
If you'll indulge a metaphor, this is not a void that can be filled - its nature is more akin to a black hole of the cosmic variety.
Feeding it things - for example, expensive ergonomic equipment - will simply add to its mass and pull.
Only if left alone might it slowly evaporate.
You must learn to live with it.
Materialism is the belief that something outside yourself will finally bring you permanent satisfaction, and we don't want to be materialistic.

Just remember why you're doing this: to facilitate work and avoid injury.

# The Setup

The main event. Here's what I ended up getting.

### Desk: UPLIFT V2 motorized adjustable standing desk

This one hurt the most: it cost $835!
I'd been spoiled with a motorized adjustable standing desk while working at Microsoft, and was loathe to give it up.
I ended up springing for an uplift desk because it drives me crazy when things are wobbly, and many standing desks are allegedly quite wobbly if you're a taller person.
I bought a [very basic uplift desk model](https://www.upliftdesk.com/uplift-v2-standing-desk-v2-or-v2-commercial/): 60x24" white laminate with just the memory pad and power grommet added on.
No way was I going to find something like this on the used market, sadly.
Buy once, cry once.
It came with a very nice cushy mat which really does improve the standing experience.

![A white uplift desk viewed from below.](uplift-desk.jpeg)

If you're looking to save money here and don't mind buying the motorized legs & desk top separately, there are allegedly [good deals to be had on Monoprice](https://lobste.rs/s/fvnhyd/taking_my_home_work_setup_seriously#c_sj3n6z).

### Chair: Haworth Zody

Here's where I got a pretty good deal.
Forget Herman Miller: their chairs still go for crazy prices used, but one can easily find a gently-used [Haworth Zody](https://www.haworth.com/na/en/products/stools/zody-1.html) for $200 (vs. $1400 new) on Craigslist in any large-ish metropolitan area.
I also grew used to these while working at Microsoft, and it's a truly wonderful high-quality chair - and very adjustable!
You can change seat height, seat depth, tilt tension (or lock it from leaning back), lumbar support height, and armrest position along all axes (height, side-to-side, forward/backward).

![Two photos of a grey computer chair from different angles.](haworth-zody.jpg)

If you're buying used you should test two things.
One, put it on a level surface and extend seat height to maximum; check whether it's developed a wobble.
Two, test whether the tilt tension control still works (adjusted with a crank on the right side).
I use it locked all the time anyway, but you might care about this feature - I'd just use its absence to secure a further discount!

The only other advice here is not to buy one of those [ergonomic kneeling chairs](https://youtu.be/x3DpVTyQOyM).
They're cheap & compact, but if you're like me your back will get tired and you'll just revert to a horrible forward-slouching posture.
If you really think you've got what it takes, try sitting on an inexpensive yoga ball first - at least you might be able to use it for other things when the whole venture goes sideways.

### Keyboard: Goldtouch GTN-0099

Let's start this section with a hot take: mechanical keyswitches are wholly unnecessary for a good keyboard experience.
I had a [Das Keyboard](https://www.daskeyboard.com/model-s-professional/) long ago (without keylabels for *extra hacker cred*), and it was nice for a bit but you quickly tune out the clickiness (although your roommates don't).
In the end it just isn't worth the huge extra cost & noise (yes I've heard of Cherry MX Silent Red switches, leave me alone you fanatics).
Mechanical keyswitches provide very dubious ergonomic benefit.
I will say it's humorous seeing tons of love & effort dumped into custom mechanical keyboards that still use a flat slab layout like they're some throwaway Dell keyboard from 2006.
Show some love toward your wrists, you barbarians!

For a number of years I used the [Microsoft Sculpt Ergonomic Keyboard](https://www.microsoft.com/en-us/p/microsoft-sculpt-ergonomic-desktop/8xk02kz6k69w?activetab=pivot%3aoverviewtab), which was nearly perfect except for the build quality being complete crap - I burned through two of them in four years (this seems to be a common story).
After the second one died I started using the [Microsoft Natural Ergonomic Keyboard 4000](https://www.microsoft.com/accessories/en-us/products/keyboards/natural-ergonomic-keyboard-4000/b2m-00012), an enormous dinosaur which can reliably be found for $20 used.
Unfortunately it has a full number pad on the right side, meaning you're either reaching way out toward your mouse or pushing the keyboard to your left and typing with asymmetric hand positions:

![Overhead photo of my arm alignment while typing on the keyboard, with lines drawn showing how asymmetric my arm position is relative to the midline, and how far I have to swing my arm to reach the mouse.](ms4k-hand-position.jpg)

I wanted a keyboard that was split/tented, no number pad, bombproof build quality, and less than $120 or so.
It came down to the [Kinesis Freestyle2](https://kinesis-ergo.com/shop/freestyle2-for-pc-us/) and the [Goldtouch GTN-0099](https://shop.goldtouch.com/products/goldtouch-v2-adjustable-comfort-keyboard-pc-only).
In the end I chose Goldtouch because it's much less expensive ($60 on eBay!) and looks decidedly less cheap than the Kinesis and its weird sold-separately tenting kit.
I couldn't be happier!
I love the Goldtouch's chunkiness & solidity.
The keyswitches are sturdy & pleasant to type on.
Also, it is very adjustable - the keyboard halves are connected by a lockable ball joint you can set to any reasonable combination of split and tent angle.

![Photo of a black tented keyboard.](goldtouch-keyboard.jpg)

It strikes me as odd that I so rarely saw mention of Goldtouch in any of the (many) discussions of ergonomic keyboards I've read in my time online.
Maybe it's because of their business-centric marketing, or the website design & name giving the impression it's run by a group of humorless physiotherapists who've never pondered the glory of a unicorn-vomit RGB lighting setup.
Realistically though, most online keyboard discussion is just driven by mechanical keyswitch enthusiasts who have no interest in their product line.
It should be noted that pretty much the only mechanical keyboards meeting my requirements were the [Kinesis Freestyle Pro](https://kinesis-ergo.com/shop/freestyle-pro/) ($210 with tenting kit), the [Kinesis Advantage2](https://kinesis-ergo.com/shop/advantage2/) ($330), and the [Ergodox EZ](https://ergodox-ez.com/) ($350).
All of these are beautifully-designed tools, but do they really justify a price multiple of up to 5x a non-mechanical Goldtouch keyboard?
I think not.
Goldtouch makes excellent keyboards and I hope they get the love they deserve.

### Mouse: Logitech MX Vertical Ergonomic mouse

Before this I had a cheap [$25 vertical mouse from Anker](https://www.anker.com/products/variant/anker-24g-wireless-vertical-ergonomic-optical-mouse/A7852011), which unsuprisingly died within a year or two of purchase.
After that I bought the more expensive [$90 vertical mouse model from Logitech](https://www.logitech.com/en-us/product/mx-vertical-ergonomic-mouse) hoping for better build quality.
It's worked out well so far!
Mousing was the source of my wrist injuries a decade ago and they haven't yet reoccurred.

![Photo of a grey vertical mouse on a black trackpad.](logitech-mouse.jpg)

My friend David (the aforementioned who programs via speech) recommends using a trackball instead of a vertical mouse.
I'm considering this route myself; of my meagre remaining ergonomic complaints, the most prominent is the very slight outer wrist discomfort I get from prolonged mousing.
It makes sense that rotating a ball would produce less stress than moving your entire hand/wrist/arm.
Supposedly Kensington is a good brand here.
I'd probably keep my vertical mouse around in case I want to play a video game with shooting mechanics, though.

### Monitors: 2x LG 24UD58-B 24" 4k IPS monitors

This (along with the uplift desk) is the only place I really splurged, after reading the post "[*Time to upgrade your monitor*](https://tonsky.me/blog/monitors/)" by Nikita Prokopov.
I was really captured by the idea that since 4k monitors are essentially just four 1080p monitors stuck together, using a 4k monitor with 2x scaling is like having a single really, really high quality 1080p monitor.
And it's true!
I love my 4k monitors.
They really do make everything - text included - look very beautiful.
Maybe even in an enduring way; I've had them for a month now and the effect hasn't worn off yet.
They were $300 each, which was surprisingly inexpensive because 4k has always felt like that new thing that hasn't quite landed yet, like 3D displays.
Nope, 4k panels are a commodity now!
This also means used 1080p monitors are basically given away for free if you want to live the many-monitor/low-DPI life.

![Photo of two monitors set up symmetrically in landscape mode. A photo of a nebula fills the screens.](dual-monitors.jpg)
{{< center "*Wallpaper sourced from [here](https://www.reddit.com/r/wallpapers/comments/711kc9/dual_4k_nebula76802160/).*" >}}

Monitor manufacturers don't seem to have caught on to the 2x scaling use case.
The [LG 24UD58-B](https://www.lg.com/us/monitors/lg-24UD58-B-4k-uhd-led-monitor) was the smallest 4k monitor I could find, at 24".
Everything else was at least 27", which is enormous!
Even 24" is quite big; ideally they'd be around 20", the size of the 1080p monitors in my setup back at the office.
Most progress in the monitor world seems to be along axes relevant for gaming & entertainment rather than productivity (refresh rate, response time, high dynamic range, wide color gamut) but I'm sure a smaller 4k monitor will be released eventually.

I was originally leaning toward buying an ultrawide monitor since they're essentially just a 16:9 dual-monitor setup without the bezel interrupting the middle - curved, too!
Sadly these all max out at 1440p vertical resolution and are intensely expensive: around $1500.
Maybe in five years I'll be ready to upgrade my monitor setup again, and a 7680x2160 ultrawide will be on the market for a reasonable price (although [this](https://www.reddit.com/r/ultrawidemasterrace/comments/d33atd/any_talk_of_7680x2160/) thread claims such resolutions are beyond DisplayPort's current capabilities so I'm not optimistic about the last point).

{{< youtube jBhQrGXYyw4 >}}

Some commenters [mentioned](https://lobste.rs/s/fvnhyd/taking_my_home_work_setup_seriously#c_vdwxl7) the ergonomic drawbacks of the standard symmetrical dual-monitor setup: your neck always has to be twisted off-center when looking at a screen.
I'll be experimenting with some different monitor setups to see what works; the idea of putting one monitor flat in the middle and another tilted to the side (even oriented vertically!) sounds like a good place to start.

### Videoconferencing & Laptop: First-gen Microsoft Surface Book + Surface Pen

These regularly show up used on Craigslist for $450-$550.
They're really [quite an incredible deal](https://support.microsoft.com/en-us/help/4488969/surface-book-tech-specs), you get a good-quality 1080p webcam plus a decent microphone & speakers and a built-in *digitizer*!
You can use the Surface Pen with the digitizer to diagram your ideas in real time for others in the call.
It's also handy to have this videoconferencing platform in laptop format, so you can choose where to take your call.
Having a laptop in general is also just convenient, verging on necessary.
This one is more than adequate as a mobile dev machine.
Its 3000x2000 screen & tablet mode also makes reading papers & textbooks pleasant, and it's very useful for marking up & signing PDFs (a greater quality-of-life capability than it sounds!)

![Photo of the surface book laptop with the screen displaying a hand-drawn demonstration diagram.](surface-book.jpg)

If you don't have a pre-existing workstation you can also buy a [Surface Dock](https://www.microsoft.com/en-us/p/surface-dock/8qrh2npz0s0p) (*definitely* buy this used) and use the Surface Book as your main dev machine.
I'd look for one of the higher-spec'd Surface Book models if you plan to go this route.
Also, a note on the dock: it was infamously buggy on release, so be sure to [update its firmware](https://support.microsoft.com/en-us/help/4023478/surface-update-your-surface-dock) before use.

### Miscellaneous

For a monitor stand I bought one of the indistinguishable $25-$35ish [metal desk clamp monitor stands](https://www.tykesupply.com/Dual_Monitor_Stands-Dual_LCD_Monitor_Stand.html); the only real requirement I had was it being tall enough to hold the monitors at eye level.
I might swap this out for a pair of single-monitor stands of the same desk clamp design, as the arms of the dual-monitor model push the monitors out a bit further from the wall than I'd like.
But then I'd probably have to buy some longer DisplayPort cables.
Something to let sit for a bit.

![Overhead photo of the monitor arms and how much space they require behind them.](monitor-arms.jpg)

For my workstation I'm just continuing to use the small form factor machine I built nearly seven years ago driven by an i5-4670k with 16 GB DDR3 RAM.
In fact, if you want to be really frugal, I actually endorse building this PC today!
The i5-4670k can easily be overclocked and the only real constraint is the expense of larger RAM sizes with DDR3.
I saw a very expensive (for the time) motherboard + i5-4670k combo go on eBay for $40 (!!!) not too long ago.
There's been [a lot of talk](https://news.ycombinator.com/item?id=23223147) about how the new AMD Ryzen 5 3600 CPU has incredible price/performance, but it's basically blown out of the water by buying something used.
Remember single-threaded CPU performance has been [basically flat for a decade now](https://www.cpubenchmark.net/year-on-year.html).

![Photo of a computer in the mini-ITX format with the enclosure removed.](workstation.jpg)

I was lucky enough to get a pair of Bose QC35 noise-canceling bluetooth headphones from work, which I use to listen to music & also as a microphone during calls (although the Surface Book's microphone array works just fine).
If I didn't have these I would probably purchase a used pair of wired Sennheiser or Audio-Technica headphones.
Truth be told my QC35s annoyingly drop/cut out regularly despite trying several different USB bluetooth adapters.
Sometimes I miss the wired life.

One last notable quality-of-life purchase was a [flat cat6 ethernet cable](https://www.amazon.com/gp/product/B01FWGK8QC) to connect my workstation to the router directly instead of through wi-fi.
It's surprisingly easy to hide this cable under the baseboard and even run it beneath carpet across door thresholds, if you have some tent poles lying around!

# Future Improvements?

At this point I've ticked the box on nearly every ergonomic gadget there is, except a desk treadmill (which I did previously own!) and a keyboard tray.
Keyboard trays, for those not in the know, put your keyboard & mouse on an adjustable tray attached to an arm that retracts along a track on the underside of the desk.
The big buzzword capability here is *negative tilt*, where you can tilt the tray down away from you to keep your wrists straight without bending your elbows so much.
Unfortunately keyboard trays tend to be (1) [wobbly as hell](https://www.youtube.com/watch?v=KX5CVnafwcc&t=49s), and (2) [expensive as hell](https://www.humanscale.com/products/product-buy.cfm?group=KeyboardSystems).
They also make the underside your desk not so roomy if you're using it for something other than typing.
There's a brand called Humanscale that seems sturdier than most, but it's very expensive (allegedly there are excellent deals on eBay).
Their flagship tray also has a very neat feature where you can elevate your mouse independently of your keyboard, useful for boards like the Goldtouch which raise your hand position as you tent them more.
If you're looking for the ultimate in adjustability you really can't beat a keyboard tray, despite their drawbacks.
I'm going to skip out on them for a while; perhaps in a year or two I might look into them again.

{{< youtube zddoDlxQa-g >}}

There's also an ergonomic issue which doesn't affect me personally but is so common it bears mention: people whose legs are too short to comfortably reach the ground no matter how they adjust their desk or chair.
The solution here is very simple, and also cheap - buy a footrest!
Stop mucking around trying to find a desk that goes low enough for your needs.
Bring the floor to you!
Alternatively, a keyboard tray would also help with this problem.

{{< youtube lnnlRKXodQc >}}

# Tallying the Damages

| Item | Cost |
| :--: | :--: |
| [Uplift Desk](https://www.upliftdesk.com/uplift-v2-standing-desk-v2-or-v2-commercial/) | $835 |
| [Haworth Zody Chair](https://www.haworth.com/na/en/products/stools/zody-1.html) | $200 |
| [Goldtouch Keyboard](https://shop.goldtouch.com/products/goldtouch-v2-adjustable-comfort-keyboard-pc-only) | $80 |
| [Logitech Vertical Mouse](https://www.logitech.com/en-us/product/mx-vertical-ergonomic-mouse) | $85 |
| [2x LG 4k IPS Monitors](https://www.lg.com/us/monitors/lg-24UD58-B-4k-uhd-led-monitor) | $600 |
| [Microsoft Surface Book](https://support.microsoft.com/en-us/help/4488969/surface-book-tech-specs) | $450 |
| [Monitor Stand](https://www.tykesupply.com/Dual_Monitor_Stands-Dual_LCD_Monitor_Stand.html) | $35 |
| Miscellaneous* | $100 |
| Total | $2,385 |

*\*Display & ethernet cables, cable management supplies, mousepad, etc.*

It adds up quick.
You could also include the hypothetical cost of recreating my existing workstation with used components, which would add around $300 or so.
We might then subtract the $350 I got from selling my existing desk & monitor on the used market.
I'm currently an independent contractor so I think this can all be deducted on my taxes (pending confirmation by a tax professional) but it still stings a bit.
If I hadn't splurged on the Uplift desk & 4k monitors I could have cut this down by $1000 or more.
Still, I'm happy with how it all turned out!

![Photo of the final setup. A white desk surmounted by two monitors sits nestled in an alcove. The alcove is painted navy blue and has a poster and several framed photos hung up. A computer chair sits with its base tucked under the desk. A workstation sits on a platform above the floor on the bottom right.](home-improvement/6-complete-home-office.jpg)
{{< center "*Poster from [Ars Obscura Bookbinding & Restoration](https://www.arsobscurabookbinding.com/?p=143) in Seattle.*" >}}

# Updates for 2022

Time is the best judge of all things, and I've now had my setup for a year and a half.
Here I'll go over what I tried, what I kept, and what I discarded.
The winners:
 * **My decision to invest in all this** - I've enjoyed using this setup for the past 1.5 years and now that I'm an independent contractor there are about even odds I will work from home for the rest of my life.
 * **The Uplift desk** - No complaints.
 I love how I can press a button to raise it and stow away my chair at the end of the day.
 I'm sure better competitors have materialized by now, although I haven't been keeping track.
 * **The Haworth Zody chair** - No complaints.
 * **The Goldtouch keyboard** - No complaints.
 Many highfalutin ergonomic mechanical keyboards have been released in the interim, but I haven't been tempted to switch once.
 Well, that's not entirely true - people keep raving about the classic Kinesis Advantage2 so much that I do kinda want to try it. It's just very expensive, even used.
 * **The 24" 4K LG monitors** - I loved these so much I bought a third.
 The middle monitor is kept in landscape, while the two on either side are in portrait mode.
 It's nice to have a center screen I can focus on while websites, log files, or documentation are displayed at great length to my left and right.
 For this I needed to ditch my old dual-monitor stand and buy a sturdy triple-monitor stand, also [from Tyke Supply](https://www.tykesupply.com/product.cgi?product=534&group=53).
 * **My old PC** - Now whirring into its eighth year, I've made some upgrades so it's about as powerful as it can get without tearing out the motherboard and power supply (at which point it would just as well be a new PC).

The losers:
 * **The Logitech MX Vertical mouse** - I switched to a trackball and haven't looked back.
 All my wrist pain disappeared.
 I am now a zealot and will never again use a pointing device that requires you to repetitively & minutely flick your wrist around.
 I use the [Kensington Pro Fit Ergo Vertical Wireless Trackball](https://www.kensington.com/p/products/ergonomic-desk-accessories/ergonomic-input-devices/pro-fit-ergo-vertical-wireless-trackball3/) and love it.
 It's more than usable for games with shooting mechanics, and may even work better for large movements like spinning around in-game.
 My only complaint is that it's wireless and needs a battery (despite sitting on my desk never moving), which Kensington rectified by later releasing a [wired version](https://www.kensington.com/p/products/electronic-control-solutions/trackball-products/pro-fit-ergo-vertical-wired-trackball4/).
 * **The Surface laptop (for video calls)** - The laptop itself is great, but I grew tired of having to context-switch to a weaker machine whenever I wanted to join a meeting.
 I ended up buying the [Razer Kiyo](https://www.razer.com/streaming-cameras/razer-kiyo) webcam, which has great video quality although the included ring light - while a nice idea - has far too harsh blue/white color-temperature to make you really look good.
 I also purchased the [Samson Q2U microphone](http://www.samsontech.com/samson/products/microphones/usb-microphones/q2u/) which works great and (so I'm told) makes me sound like a podcaster.
 * **Bluetooth headphones** - I went through three separate bluetooth dongles but none of them worked - not connecting, losing connection, audio stuttering.
 It was also annoying not being able to be on video/voice call and listen to computer audio simultaneously while playing video games with my long-distance friends.
 I ended up buying a pair of wired [Sennheiser HD 598](https://en-us.sennheiser.com/audio-headphones-high-end-surround-sound-hd-598) open-ear circumaural headphones (used from ebay).
 The open-ear design works well on video calls, because you get natural feedback on the level & tone of your voice as you speak.
 
I also purchased a few accessories:
 * A flexible-neck desk clamp lamp with adjustable brightness & color temperature.
 Works well for lighting the desk area for reading or writing, or blasting the back wall for backlighting.
 * An electric high-pressure computer duster [from CompuCleaner](https://www.itdusters.com/product/compucleaner/)
 * A new [Tripp Lite 12-outlet surge protector](https://www.tripplite.com/protect-it-12-outlet-surge-protector-8-ft-cord-2160-joules-tel-modem-protection~TLP1208TEL/) so I can power & protect all of this

The new damage totals to:

| Item | Cost |
| :--: | :--: |
| [LG 4k IPS Monitor](https://www.lg.com/us/monitors/lg-24UD58-B-4k-uhd-led-monitor) | $300 |
| [Triple Monitor Stand](https://www.tykesupply.com/product.cgi?product=534&group=533) | $60 |
| PC Upgrades* | $400 |
| [Kensington Trackball Mouse](https://www.kensington.com/p/products/ergonomic-desk-accessories/ergonomic-input-devices/pro-fit-ergo-vertical-wireless-trackball3/) | $70 |
| [Razer Kiyo Webcam](https://www.razer.com/streaming-cameras/razer-kiyo) | $100 |
| [Samson Q2U Microphone](http://www.samsontech.com/samson/products/microphones/usb-microphones/q2u/) | $70 |
| Microphone Stand & Pop Filter | $30 |
| [Sennheiser HD 598 Headphones](https://en-us.sennheiser.com/audio-headphones-high-end-surround-sound-hd-598) | $90 |
| Desk Lamp | $40 |
| [Computer Duster](https://www.itdusters.com/product/compucleaner/) | $50 |
| [Surge Protector](https://www.tripplite.com/protect-it-12-outlet-surge-protector-8-ft-cord-2160-joules-tel-modem-protection~TLP1208TEL/) | $40 |
| Miscellaneous** | $50 |
| Total | $1,300 |

*\*Upgrade CPU & GPU, add 1 TB SSD, replace fans*

*\*\*USB hub, various cables & adapters, etc.*

Those details really add up!
Combined with the original, this comes to a whopping $3,685!
It's been fairly static for most of the last year though, so I don't anticipate spending much more than this in the near future.

What about the far future?
The obvious change would be to upgrade my ageing PC, and to that end I've been idly spec'ing out a replacement Linux workstation from [System76](https://system76.com/).
Lately however I've been reading [[1]](https://news.ycombinator.com/item?id=28678041) [[2]](https://news.ycombinator.com/item?id=29978036) about VR workstations.
The headsets can be expensive, but they render most of my setup unnecessary!
No need for a standing desk, chair, monitors, etc.
So a few years from now I might see my home work setup evolve in form as well as function.
The [Simula One](https://simulavr.com/) VR computer looks particularly interesting.
This could also involve a full change in my ergonomic design approach, similar to the one outlined in this video:

{{< youtube UPZa8yMLfWw >}}

The presenter speaks about the importance of changing postures throughout the workday rather than finding a single ideal posture.

# Other Reading & Watching

 * [Hacker News](https://news.ycombinator.com/item?id=24169729) and [Lobsters](https://lobste.rs/s/fvnhyd/taking_my_home_work_setup_seriously) discussions for this post - the latter has some great advice on the drawbacks of dual-monitor setups and how to save money on a motorized adjustable standing desk.
 * [*Building the Ultimate Home Office (Again)*](https://www.troyhunt.com/building-the-ultimate-home-office-again/) by Troy Hunt, a somewhat more maximalist approach to this same problem; inspired me to write this! [[HN Discussion](https://news.ycombinator.com/item?id=23938124)]
 * [*Notes on 416 Days of Treadmill Desk Usage*](https://www.nealstephenson.com/news/2015/03/09/notes-on-416-days-of-treadmill-desk-usage/) by Neal Stephenson (the author), the post that originally inspired my purchase of a desk treadmill five years ago (later sold during a move) and may one day inspire it again. [[HN Discussion](https://news.ycombinator.com/item?id=9174746)]
 * [*Time to upgrade your monitor*](https://tonsky.me/blog/monitors/) by Nikita Prokopov, this post had quite an effect on me for reasons I've been unable to discern. [[HN Discussion](https://news.ycombinator.com/item?id=23551983)]
 * [*The Ergonomics Guy*](https://www.youtube.com/user/TheErgonomicsGuy): a very valuable if underproduced YouTube channel run by a physical therapist named Steve Meagher; features general advice, overviews of ergonomic device categories, and specific product reviews.
 * [*Linus Tech Tips*](https://www.youtube.com/user/LinusTechTips): another YouTube channel that reviews many, many pieces of computer equipment with lots of context & detail.

 # Bonus: Some Home Improvement

 Upon moving into this Atlanta apartment (sight unseen after driving from Seattle) I saw the nook I'd planned to use for my home office was occupied by a permanent desk.
 Since all the units in the building were being renovated and ours was still in the old style, I got permission from building management to tear out the desk & do with the nook as I pleased.
 After consulting my wonderful & talented sister Carolyn Helwer ([who works as an interior designer!](https://www.blockinc.ca/carolyn-helwer-bio)) I painted it a nice navy colour ("[Salty Dog](https://www.sherwin-williams.com/homeowners/color/find-and-explore-colors/paint-colors-by-family/SW9177-salty-dog)" from Sherwin-Williams) and ended up loving the result.
 It took a lot of coats of paint!

![Photo of a nook filled with a truly awful desk, mounted to the wall.](home-improvement/1-permanent-desk.jpg)
{{< center "*The desk before being torn out.*" >}}

![Photo of the same nook with the desk torn out, with some damage to the wall.](home-improvement/2-desk-torn-out.jpg)
{{< center "*Success! Wall required a good bit of drywall mud & sanding.*" >}}

![Photo of the nook with the wall damage filled in and prepped with painter's tape & plastic sheeting.](home-improvement/3-paint-prep.jpg)
{{< center "*Prepped for paint.*" >}}

![Photo of the nook now painted a mottled blue color, clearly still needing some additional coats.](home-improvement/4-partially-painted.jpg)
{{< center "*After one coat. It took three coats + primer to get a good solid colour!*" >}}

![Photo of the author standign in front of the fully painted and cleaned up nook.](home-improvement/5-final-product.jpg)
{{< center "*Proud of the result! Channeled my teenage summer job painting apartment interiors.*" >}}
