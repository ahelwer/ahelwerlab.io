---
title: FOSS I Love
subtitle: Local game streaming with Sunshine and Moonlight
date: 2023-04-29
bigimg: [{src: "troll.jpg", desc: "Sculpture from the \"Trolls: Save the Humans\" art exhibit by Thomas Dambo at the Atlanta Botanical Gardens, GA, USA" }]
image: "/post/2023-04-29-sunshine-moonlight/troll.jpg"
---

Here I'll write an ode to two closely-linked FOSS projects that have, recently, absolutely floored me with their competence & quality: [Sunshine](https://app.lizardbyte.dev/?lng=en) and [Moonlight](https://moonlight-stream.org/).
These are used to stream video games over a (usually local) network.
Sunshine is a game streaming server: it runs on your PC as it chugs away doing all the heavy lifting of running the game itself, while Moonlight is a game streaming client that runs on whatever thin hardware exists where you want to play the game!
I run Sunshine on my home office workstation and Moonlight on a NVIDIA Shield Android TV connected to a 55" screen in the living room.
Some games are just best played on the couch!
It's also important if you have someone you want to share the experience with instead of sequestering yourself alone in your dark Gamer Den.

### Five Years of Frustration

Before Sunshine & Moonlight came along I'd spent half a decade trying to find a decent local game streaming solution.
I had a modest but acceptable rig I could use for 1440p gaming targeting 60 fps on most games, and didn't want to deal with the hassle of buying an extra console for the TV with its own exclusive games library.
I tried Steam Link, but the hardware was limited to 1080p and the stream had noticeable artifacting.
I tried a used Xbox One S for its alleged local Windows streaming client capabilities, but it was utterly unplayable (and re-sold shortly thereafter).
Finally I bought a used NVIDIA Shield Android TV.
The NVIDIA GeForce Experience software on Windows (usually used for auto-updating GPU drivers) works as a local game streaming server if you have a NVIDIA GPU.
There's a corresponding NVIDIA-authored client you can run on the NVIDIA Shield.
Together these worked... *okay* for a couple years.
However, a few years ago the local game streaming software hit soft-EOL and stopped being updated.
Bugs (which were numerous) went reported and unfixed as NVIDIA focused on cloud streaming instead.
I tried switching back to Steam Link (which by now had taken the form of an Android TV app instead of a hardware box) but the artifacting issues remained.
Enter Sunshine and Moonlight!

After reading through online threads filled with increasingly frustrated people in the same position as me, I saw someone recommend Moonlight.
An alternative game streaming client?
Released under GPLv3?
It seemed too good to be true, but it really did work much better than the NVIDIA client!
I connected to the same NVIDIA GeForce Experience server on my PC, and two hideously buggy programs trying to talk to one another became one hideously buggy program talking to a very thoroughly solid free & open source program.
You could really feel the love that went into Moonlight: it had all the features, all the stat displays, all the *solidity* that can only come from a group of people being immensely frustrated with the gap between what could be and what is, and choosing to invest their time making it a reality.

This setup trucked along uneasily for a bit over a year, Moonlight doing its best to wrangle the unhinged monstrosity of the NVIDIA local game stream server.
In late 2022 though I'd truly had enough of Windows and made the jump to Linux full time.
This also entailed a switch to an AMD GPU, since I wanted to run Wayland using the open-source GPU drivers.
Thus came the demise of my entanglement with the NVIDIA local game stream server.
What could replace it?
Here I finally discovered Sunshine, another GPLv3 project started in early 2020 with the intention of fully replacing the NVIDIA local game stream server (and adding support for AMD GPUs).
Would this be as much of a home run as Moonlight was?
Sadly, when I first tried it in November 2022, the answer was no.
Every game I tried had copious frame drops and very noticeable latency.
I took a long hiatus from video games after that, as work & personal projects took over.
Until...

### Apotheosis

In early April I had a hankering for video games again.
I again set up Sunshine and... it was perfect!
Miracles were worked in the past six months and I have absolutely no notes.
Zero dropped frames, 10ms network latency over wired cat5e through a Turris Omnia router with 2ms decoding latency.
It supports 4k resolution, 120fps, and HDR (not that I can take advantage of it).
I played through the entirety of Hollow Knight, a very latency-sensitive game, using a wireless Xbox One controller connected to the NVIDIA Shield via bluetooth no less.
The true test of any game streaming setup is whether you can beat [Nightmare King Grimm](https://youtu.be/7JWQ9KjwPyE), and I did!

I am utterly amazed by the accomplishment of the FOSS community here.
A half-decade exercise in frustration brought to an end because people wanted it bad enough and did the hard work of making it a reality!

### What's next?

I was fibbing a bit when I said that I switched to Linux full-time.
I kept a dual-boot setup for Windows (just for that extra bit of difficulty during the Arch install) and continue to keep all my games there.
Linux gaming is pretty incredible these days with the advent of the Steam Deck, so I might actually go *full-time* full-time Linux in the near future.
I don't view it as being particularly important since video games are all proprietary software anyway.
The few FOSS (modulo assets) ones I can even think of that I personally enjoy are:
 * The Elder Scrolls III: Morrowind (2002) via [OpenMW](https://openmw.org/en/)
 * The Marathon Trilogy (1994-1996) via [Aleph One](https://alephone.lhowon.org/)
 * Age of Empires II (1999) via [OpenAge](https://openage.sft.mx/)

I could also fully FOSSify the streaming client by switching from the NVIDIA Shield Android TV to the [OSMC Vero 4K+](https://osmc.tv/vero/) if I wanted, but I still use Netflix and stuff so I'm not quite ready to make that leap.

For now I'm just enjoying the smooth experience of quickly starting up a game & streaming it to the couch without having to repeatedly reboot both my PC & NVIDIA Shield while praying for it to connect.
All my praise to everybody who contributed to Sunshine and Moonlight!

### Discussions

 * [lobste.rs](https://lobste.rs/s/mp1erj/foss_i_love_local_game_streaming_with)
 * [Hacker News](https://news.ycombinator.com/item?id=35753415)
 * [r/programming](https://old.reddit.com/r/programming/comments/132wciq/foss_i_love_local_game_streaming_with_sunshine/)

