---
title: Breaking my college Paxos project with TLA⁺ and model-based testing
subtitle: Refinement checking with lightweight formal methods
date: 2020-09-26
bigimg: [{src: "thundercloud.jpg"}]
tags: ["distributed systems", "formal methods"]
draft: true
image: "/post/2020-09-26-paxos-mbt/thundercloud.jpg"
---

I've always been grateful to have attended the University of Calgary, a school few people ever consider except by reasoning that such a place would exist upon hearing of it.
Our undergraduate CS department produced James Gosling and Theo de Raadt of Java and OpenBSD/OpenSSH fame respectively, but when I arrived in 2008 it was going through a bit of a lull in enrollment.
So it was that certain especially interested students were given substantial leeway in the conduct of their studies by some of the department's professors, replacing some or all of a course's assignments with a single large self-directed project in the area of study.
One of my friends used the assembly course to implement a simple flight simulator that included 3D graphics.
I used the networking course to implement the multi-Paxos protocol.
This project, especially by virtue of having been self-directed, was so influential to my future career and interests that I will remain eternally grateful to those professors who were willing to put in the work of dealing with marking non-standard projects (specifically [Dr. Mea Wang](http://networks.cpsc.ucalgary.ca/mea/), thank you!).
Here's my old demo video of the project:

{{< youtube jyel-iADuUU >}}

Now that the formalities are out of the way, let's apply all those interests in beating the crap out of it - seven years later.

## Formal verification is a lot of work

I'm a big fan of formal verification.
It's more than ready to make the leap from projects funded by public research grants to projects funded by companies trying to save money - if not in initial development, at least in maintenance & reliability.
The initial development cost is real.
Some state-of-the-art numbers come from Microsoft's IronFleet project, which reports an overhead of 7.7 lines of proof annotation for each line of code [[pdf](https://www.microsoft.com/en-us/research/wp-content/uploads/2017/06/ironfleet-cacm.pdf)].
This is at least within an order of magnitude of lines of conventional test code for a high-assurance project, which is optimistically double or triple the number of lines of project code - although much of that will be boilerplate.
Proof annotation is decidedly not boilerplate.
The pool of developers ready to start working on such a thing is quite small, relatively speaking.
Still, I have great faith in the working software engineer's ability to teach themselves truly interesting topics, like how to prove code correct, if they will actually be able to use it at their job.
The cultural success of Rust demonstrates there is great appetite for such ideas.

You can handwavily view software QA methods as lying on a spectrum with formal verification at one end and testing in production at the other (snark).
In between lies every testing methodology you've ever heard of, and they're all trying to do the same thing: [ensure the code implements the spec](https://ahelwer.ca/post/2018-02-12-formal-verification/).
This post occupies a space directly adjacent to formal verification, which we might call "lightweight" formal methods.
With lightweight formal methods you still have a formal *specification*, likely written in a developer-friendly language like TLA⁺, but there isn't an inexorable chain of truth connecting that spec to the actual code.
People get really hung up on this but there's a tremendous cost/benefit ratio just in creating an unambiguous machine-checkable specification.
Software engineering is about turning requirements/specifications into code.
An enormous amount of attention has been lavished on the code end of that process; there's still a lot of low-hanging fruit on the specification side.

Still, it would be nice to have some way of connecting our code to spec beyond our professional wits.
Here we arrive at Model-Based Testing, henceforth called MBT.
MBT takes advantage of the structure of modern formal specifications, which have invariable settled on modeling the system as some flavor of state machine (even if it might not immediately resemble such).
The idea is simple:
 1. Create a bunch of abstract execution sequences on your formal spec's state machine
 2. Translate those execution sequences into operations to run on your actual code
 3. Run the operations on your code, recording the system state after each step
 4. Translate the system states back for comparison with your formal spec's state machine
 5. State mismatch? This is a bug, and you've recorded the steps leading directly to it!

If you're familiar with [property-based testing](https://hillelwayne.com/post/hypothesis-oracles/) this bears some resemblance.
There are few things less interesting than taxonomical classification of testing methodologies (as anyone who has partaken in the "is this a unit test" debate will attest) so let's just acknowledge some conceptual overlap and move on.
In my view MBT occupies the space between property-based testing and formal verification on our hypothetical testing continuum seen above.
