---
title: UEFI, TPMs, Secure Boot, and All That
subtitle: What's the Benefit?
date: 2022-12-01
bigimg: [{src: "georgia-autumn.jpg", desc: "Fall foliage in Sawnee Mountain Preserve, GA, USA" }]
tags: ["practices"]
draft: true
image: "/post/2022-12-01-uefi-tpm-fde/georgia-autumn.jpg"
---

Lately I've been taking the time to get really into "Linux".
The first eight years of my career were spent at Microsoft so I became very comfortable using Windows for development and daily tasks.
However, I've grown weary of Windows' constant attempts to run roughshod over my consent.
It tries to nag or trick me into making Edge the default program for everything.
It takes over my computer on login and forces me to click through various screens pushing Edge, data harvesting, Cortana - complete with the hated "enable/maybe later" response options (where's the "FUCK OFF FOREVER" option?) - and finishing with a smarmy screen telling me "Windows is all yours!" as though the computer I built & have used for over eight years is not mine.
And I guess it really isn't mine, as long as the software I use on it represents Microsoft's interests instead of my own.
Or at least it represents the interests of some KPI-brained PMs on the Edge and Cortana teams trying to *Measure What Matters* their way to a juicy rewards period.
Naturally their PowerBI dashboards can't track the people who consequently ditch Windows in disgust; second-order effects are beyond the grasp of these Data-Driven dipshits.
One real regret about no longer working at Microsoft is I can't find who these people are and send them hatemail just short of unhinged enough to get fired.
Absent that I have to settle for the next best thing, dual-booting Linux.

This isn't my first time setting up lin-win dual-boot but it was my first time getting it to work in UEFI/GPT instead of BIOS/MBR as in my college days.
If those terms mean nothing to you then you're in the right place, because until a few weeks ago they meant nothing to me either.
I've invested time figuring out the meaning of Basic Input/Output System (BIOS), Master Boot Record (MBR), Unified Extensible Firmware Interface (UEFI), GUID Partition Table (GPT), Trusted Platform Module (TPM), Full-Disk Encryption (FDE), Secure Boot, and how they all relate to one another.
This post will be of use to you if you've ever want to set up a dual-boot system, you're interested in setting up FDE or secure boot and understanding what they protect against, or if you might find yourself debugging boot issues on a computer in a datacenter halfway around the world by SSHing into the server rack's power supply to read serial port output while RDPing into the datacenter's jumpbox while RDPing into your office computer from your laptop at home, at 10pm on a Saturday.
Such things happen in our modern world.

Unfortunately, I think you really do have to have a basic understanding of UEFI if you want to set up lin-win dual-boot.
I tried to cruise through the Ubuntu graphical installer last year and only succeeded in trashing both my linux & windows installs, having to totally reinstall everything.
I now understand exactly what I did wrong retrospectively, but at the time I was so far from having any idea about how to fix things that I was searching random error messages and finding stackoverflow answers emphatically telling the asker to stop what they are doing because of how off base they were.
At that point I experienced a deep regret and viscerally understood how days of debugging can save minutes of learning.
No more! I decided to take the true learner's approach and install Arch Linux, mostly because the wiki is so good.

## UEFI/GPT vs. BIOS/MBR

The important part here is that BIOS/MBR is old and unspecified, while UEFI/GPT is the new open fully-specified way of doing things.
The switch happened some time around the release of Windows 8 in 2012.
Basically MBR and GPT are two different ways of recording information about disk partitions on the disks themselves.
You still occasionally run into MBR-partitioned disks, but there are lots of tools to convert them to the newer-format GPT-partitioned disks.
Lots of people use BIOS to refer to "the options screen that pops up when you mash F2, F9, Delete, or some other key when the computer is booting" but if you're a pedant this isn't correct - that options screen is now called the Firmware Interface.
BIOS and UEFI are types of firmware for your computer.
The BIOS had an old and bad method of orchestrating the boot process by reading some inscrutible data located near the start of a disk.
It has been superseded by the UEFI standard, which has a lot of improvements about how it orchestrates the boot.
For one, instead of having to fiddle with some weird data at the start of the disk to load a bootloader that loads another bootloader, now everything to do with boot just lives as config files & binaries in a disk partition called the EFI System Partition (ESP) that can be mounted and modified from within your running OS just like any other partition.
Also, all the boot configuration variables (boot order, etc.) that you'd usually have to change by rebooting and mashing F2/F9/Delete can be modified from within the running OS.
It's a much nicer & much more transparent experience; if you want to know more about UEFI and how it compares to the BIOS, I recommend the excellent 2014 blog post [*UEFI boot: how does that actually work, then?*](https://www.happyassassin.net/posts/2014/01/25/uefi-boot-how-does-that-actually-work-then/) by Adam Williamson.

## Dual-Booting in the UEFI Era

Windows will have created a ESP (EFI System Partition, remember) when it was installed.
By default this is a 100 MB partition right at the start of your Windows disk.
This partition size is annoyingly small and not really large enough for a good long-term dual-booting experience.
You'll be able to get things up and running, but eventually your package manager will try to upgrade your kernel and will start complaining about how it's out of space.
More on that later.

Dual-booting Linux is really as conceptually simple as creating the partition where your Linux root filesystem will live, and dropping some config files & binaries (kernel, boot manager) into the already-existing ESP.
These live happily alongside the existing Windows files, and you can easily modify it all from inside your running OS or a live image booted from a USB stick.

Lots of people recommend installing Linux on a separate disk from Windows, which is a good idea, but unfortunately is where I went awry with the Ubuntu graphical installer last year.
The Ubuntu installer failed to detect the existing ESP and created a *new, second ESP* on the disk where I was installing Linux.
Windows [doesn't like this](https://learn.microsoft.com/en-US/troubleshoot/windows-client/windows-security/cannot-boot-windows-on-primary-hard-disk-uefi).
With my current knowledge I suspect I could have fixed the problem by just copying the Windows boot binaries from the old ESP to the new ESP, but this reasoning was beyond me at the time.

When your Windows-created 100 MB ESP inevitably runs out of space, you'll want to extend it.
Unfortunately, Windows creates the ESP right at the start of the drive where it runs up against a critical 16 MB Windows-reserved partition and the start of the Windows partition itself.
Tools certainly exist to bulldoze all obstacles and move these around, but I recommend using Windows' [built-in disk management tool](https://learn.microsoft.com/en-us/windows-server/storage/disk-management/overview-of-disk-management) to shrink the main volume and create a ESP at the *end* of the drive, right before the recovery partition.
This isn't the BIOS/MBR days anymore!
The ESP doesn't have to live at the start of the drive, and you should take advantage of this flexibility.
Create a nice 512 MB area of free space, then format it as an ESP with your favorite tool.
Copy all the files from the old ESP to the new ESP, update your `/etc/fstab` file in Linux, and update the boot order from your motherboard's firmware interface or using [efibootmgr](https://man.archlinux.org/man/efibootmgr.8.en).
Keep the old ESP around as backup, or delete it.
It's annoying to have an unused 100 MB partition, but less of a headache than (probably) reinstalling Windows.

The Arch wiki has [a great article](https://wiki.archlinux.org/title/Dual_boot_with_Windows) on lin-win dual-booting, including details on the necessity of disabling fast boot and wake-on-LAN in Windows.

## Secure Boot


